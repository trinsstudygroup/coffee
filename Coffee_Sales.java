/*
Name: Christopher Hurst
Class: CSC240
Due Date: 04/12/17
Files: Coffee_Sales.java, Sales.java
Description: This program prompts the user for the number of bags and pounds the 
    cust is wanting to buy then it outputs the data
 */
package coffee._sales;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author Tallyhoe
 */
public class Coffee_Sales {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        DecimalFormat DF = new DecimalFormat("0.00"); 
        Sales trans = new Sales();
    
        String redo = "";
        do{
            System.out.println("Welcome to the Coffee Sales Tracker");
            System.out.println("\nThis program will calculate the amount due");
            System.out.println("for your coffee sales transactions");
       
            System.out.print("\nAre you ready to begin Y/N? ");
            String data = input.next();
            data = data.toUpperCase();
            switch(data){
                case "Y":
                    trans.Sales();
                    System.out.print("\nEnter number of bags: ");
                    int bag = input.nextInt();
                    System.out.print("Weight per bag: ");
                    int weight = input.nextInt();
                    trans.Sales(bag, weight);
                    trans.printSale();     
                    break;
                case "N":
                    
                    return;
            }
        
            System.out.print("\nDo you have another sale to ring up Y/N? ");
            redo = input.next();
            redo = redo.toUpperCase();
        }while(redo.equals("Y"));
        
        System.out.println("Thank you fand have a great day. Goodbye");
        
        
    }
}