
package coffee._sales;

import java.text.DecimalFormat;
import java.util.Calendar;

public class Sales {
    
    DecimalFormat DF = new DecimalFormat("0.00");
    
    
    private static double pricePerPound = 5.99;
    private static double tax = .0725;
    
    private static double totalPrice;
    private static double totalPriceWithTax;
    private static double totalTax;
    
    private static int numBags;
    private static int bagWeight;
   
    public void Sales(){
        numBags = 1;
        bagWeight = 1;
    }
 
    public void Sales(int n, int w){
        numBags = n;
        bagWeight = w;
    }
  
    public static double getTotalPrice(){
        
        totalPrice = bagWeight * numBags * pricePerPound;
        
        return totalPrice;
    }
    public static double getTotalPriceWithTax(){
        
        totalPriceWithTax = totalPrice + totalTax;
        
        return totalPriceWithTax;
    }
    
    public static double getTotalTax(){
                
        totalTax = (totalPrice * tax);
        
        return totalTax;
    }
   
    public void printSale(){
        
        String date = "";
        Calendar cal = Calendar.getInstance();
   
    switch(cal.get(Calendar.MONTH)){
        case 0:
            date = "January";
            break;
        case 1:
            date = "February";
            break;
        case 2:
            date = "March";
            break;
        case 3:
            date = "April";
            break;
        case 4:  
            date = "May";
            break;
        case 5:
            date = "June";
            break;
        case 6:
            date = "July";
            break;
        case 7:    
            date = "August";
            break;
        case 8:    
            date = "September";
            break;
        case 9:    
            date = "October";
            break;
        case 10:    
            date = "November";
            break;
        case 11:   
            date = "December";
            break;
    }
    
    System.out.println("\nToday's Date:     " + date + " " + cal.get(Calendar.DAY_OF_MONTH)
        + ", " + cal.get(Calendar.YEAR));
    System.out.println("Number of Bags:     " + numBags);
    System.out.println("Wieght per Bag:     " + bagWeight);
    System.out.println("Price per Pound:    " + pricePerPound);
    System.out.println("Sales Tax:          " + DF.format(tax * 100) + "%");
    System.out.println("Sales sub-total:    " + DF.format(getTotalPrice()));
    System.out.println("Total Tax:          " + DF.format(getTotalTax()));
    System.out.println("\nTotal Sale:         " + DF.format(getTotalPriceWithTax()));
    }


}
